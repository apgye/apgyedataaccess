set timezone TO 'America/Buenos_Aires';
CREATE TABLE submission
(
  id_submission SERIAL PRIMARY KEY,
  iep TEXT NOT NULL,
  jurisdiction TEXT NOT NULL,
  locality TEXT NOT NULL,
  datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  user_id TEXT NOT NULL,
  iep_person_name TEXT NOT NULL,
  observations TEXT,
  oficial_email TEXT NOT NULL,
  alternative_email TEXT,
  data_interval_start TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  data_interval_end TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  case_category TEXT,
  operation TEXT NOT NULL,
  enabled BOOLEAN,
  json_input JSON NOT NULL,
  input BYTEA NOT NULL,
  migration_info TEXT
);
CREATE TABLE file
(
  id_file SERIAL PRIMARY KEY,
  content BYTEA,
  filename TEXT NOT NULL,
  size INTEGER NOT NULL,
  hash TEXT
);

CREATE TABLE resubmission
(
    previous INTEGER NOT NULL,
    new INTEGER NOT NULL,
    reason TEXT NOT NULL,
    operation TEXT NOT NULL,
    iep TEXT NOT NULL,
    user_id TEXT NOT NULL,
    datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL
);
CREATE SCHEMA "primary";

CREATE TABLE mui_radicacion
(
    "Radicación" TEXT,
    organismo TEXT
);
